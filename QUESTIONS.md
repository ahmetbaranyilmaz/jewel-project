**Please view this document at https://stackedit.io. Just copy contents and paste it in left pane.**

---
##### Question 1: Project is separated to 3 modules. What is the aim of creating multiple modules for a project? Explain.
##### Answer:     Proje takibinin daha kolay olması ve farklı işlevi olan ortamları birbirinden ayırmaya yarar. Geliştirme kolaylığı sağlar.
---
##### Question 2: In root module's `pom.xml` file, we defined dependencies between`<dependencyManagement></dependencyManagement>` tags. Child modules dependencies has no version and wrote between `<dependency></dependency>` tags. What is difference between `<dependencyManagement>` and `<dependency>` tags? Explain.
##### Answer:     root pom.xml'de <dependencies içinde belirtilen kütüphanler her zaman alt modüllere bağımlılık olarak ilave edilir. <dependencyManagement bölümünde belirtilenler ise yalnızca alt modüllerin kendisi, <dependencies bölümünde belirttiyse alt modüle dahil edilir. Bu, her alt modül için tek tek sürüm belirtmeden kolayca root pom.xml den ortak olan değerleri almaya yarar.
---
##### Question 3: What are Kafka's `replication-factor` and `partitions`? Why we can't increase number of partitions and replica in single broker? Explain.
##### Answer:     Replication-Factor verinin farklı makinelerde kopyalanma sayısı. Partitions veriyi bölme miktarı. Single brokerda 1 den fazla değer verememizin sebebi tek makine olmasından kaynaklı. Tek makine olduğu için kopyalanma saysını arttırmanın bir mantığı olmayacaktır ve yine tek makine olduğu için iş yükü aynı makinede dağılacağı için partition sayısını arttırmanında bir mantığı yoktur.
---
##### Question 4: Why can't we run Kafka without Zookeeper? Explain.
##### Answer:     Çünkü Zookeeper Kafka'nın kaynak kontrolünü, ayarlarını ve dağıtık çalışmasını sağlıyor. Bazı Dağıtık çalışan sistemlerde bunu kendi içinde çalışan sistemler ile yapabiliyor fakat kafkanın şu an böyle bir özelliği olmadığı için zookeeper olmadan çalışması mümkün değil.
---
##### Question 5: Increasing the parallelism parameter of Apache Spark does not increase Kafka's consumer parallelism. Explain why.
##### Answer:	  Cevabı bilmiyorum fakat yorumlarsam ikisininden birbirinden bağımsız olmasından dolayı derim.
---
##### Question 6: In `TrainerMain` class, after loading data from MongoDB with Spark, it doesn't comes as a `RDD` class but as `Dataset<Row>`at line 37. Explain why.
##### Answer:	  Kod içerisindeki toDF() kısımından dolayı olabilir.
---
##### Question 7: In `TrainerMain` class there are few variables constructed from`StringIndexer` class.  What is the aim of `StringIndexer` class? Do we must to use it? Explain.
##### Answer:     StringIndexer 'in amacı kategorik String değerlerini, kategorik indexlere dönüştürmek. Eğer değerlerimiz zaten numerik ise kullanmamıza gerek yoktur. Kategorik sütündaki her bir kategorik sınıfın tekrarlanma sayısını hesaplıyor. En çok tekrarlanandan ez az tekrarlanana doğru sıralayıp her bir kategoriye 0 'dan başlayarak artan sayılar atıyor.
---
##### Question 8: In `TrainerMain` class there is a variable called `oneHotEncoderEstimator` which is constructed from `OneHotEncoderEstimator`. What is the aim of `OneHotEncoderEstimator` class?  Explain.
##### Answer:	  OneHotEncoderEstimator makine öğrenmesinde veri hazırlığı aşamasında kategorik niteliklerin Vektör haline getirilmesi amacıyla kullanılır.
---
##### Question 9: In `TestMain` class there is a variable called `metrics` which is constructed from `RegressionMetrics`. What is the aim of `RegressionMetrics` class?  Explain.
##### Answer:     Cevabı bilmiyorum fakat koddan gördüğüm kadarıyla bence "Root mean squared error" , "Mean absolute error", "Mean squared error" gibi değerleri görebilmek için. Bu değerlerin durumuna görede girdiğimiz hyper parametlerde değişiklik yapabiliriz belki.
---
##### Question 10: In `TestMain` class there are few `println` commands to write regression metrics to screen. Replace metrics output value to zero value below:
##### Answer:
| Parameter |Output
|--|--|
|R2| 0 |
|Root Mean Squared Error| 0 |
|Mean Absolute Error| 0 |
|Mean Squared Error| 0 |