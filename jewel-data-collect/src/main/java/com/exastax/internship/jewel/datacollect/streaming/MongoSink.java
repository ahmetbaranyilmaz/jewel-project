package com.exastax.internship.jewel.datacollect.streaming;

import com.mongodb.spark.MongoSpark;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.VoidFunction2;
import org.apache.spark.streaming.Time;
import org.bson.Document;

public class MongoSink implements VoidFunction2<JavaRDD<Document>, Time> {
    @Override
    public void call(JavaRDD<Document> documentJavaRDD, Time time) throws Exception {

        //TODO: --TAMAMLANDI-- Save RDD to Mongo Collection --TAMAMLANDI--
        MongoSpark.save(documentJavaRDD);
    }
}
