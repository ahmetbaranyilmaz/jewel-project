package com.exastax.internship.jewel.datacollect.streaming;

import com.exastax.internship.jewel.core.JewelItem;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.spark.api.java.function.Function;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.net.URL;

public class URLResultMapFunction implements Function<ConsumerRecord<String, String>, JewelItem> {

    @Override
    public JewelItem call(ConsumerRecord<String, String> record) throws Exception {
        final URL productURL = new URL(record.value());
        final Integer id = Integer.parseInt(record.key());
        final Document document = Jsoup.parse(productURL, 5000);

        /*
            Collect main attributes for product
         */
        final int price = Integer.parseInt(document
                .getElementsByClass("sale-price")
                .get(0).getElementsByClass("price").attr("data-default").replace(".", ""));

        final String[] mainAttributes = document.getElementsByAttribute("itemprop")
                .get(5)
                .text()
                .split(" ");

        final double weight = Double.parseDouble(mainAttributes[0]);
        final int carat = Integer.parseInt(mainAttributes[2]);

        /*
            Use table values
         */
        final Elements otherAttributes = document
                .getElementsByClass("product-extras")
                .get(0)
                .getElementsByTag("table")
                .stream().filter(element -> element.attr("class").equals("hidden-xs")).findFirst().get()
                .getElementsByTag("tr").get(1).getElementsByTag("td");

        //TODO: --TAMAMLANDI-- Fill class by 'otherAttributes' variable --TAMAMLANDI--

        // içinden gelen veriyi görmek için bu kısmı gösterdim sonrasında ona göre parse işlemi yaptım.
        System.out.println(otherAttributes.get(0));
        System.out.println(otherAttributes.get(1));
        System.out.println(otherAttributes.get(2));
        System.out.println(otherAttributes.get(3));
        System.out.println(otherAttributes.get(4));

        // Bundan daha doğru bir yöntem vardır büyük ihtimalle fakat benim yapabildğim çözüm bu oldu.

        String diamondType = String.valueOf(otherAttributes.get(0))
                .replace("<td>","")
                .replace("</td>","");
        //diamondType = ?
        double diamondWeight = Double.parseDouble(String.valueOf(otherAttributes.get(1))
                .substring(4,7)
                .replace(",","."));
        //diamondWeight = ?
        String diamondColor = String.valueOf(otherAttributes.get(2))
                .replace("<td>","")
                .replace("</td>","");
        //diamondColor = ?
        String diamondClarity = String.valueOf(otherAttributes.get(3))
                .replace("<td>","")
                .replace("</td>","");
        //diamondClarity = ?;
        String diamondShape = String.valueOf(otherAttributes.get(4))
                .substring(16)
                .replace("</td>","");
        //diamondShape = ?
        return new JewelItem(id, price, weight, carat, diamondType, diamondWeight, diamondColor, diamondClarity, diamondShape);
    }
}
